5library("dplyr")
library("ggplot2")
library("readr")

# Read in csv, from kaggle data set description:

# "Each row in the dataset corresponds to a property that was inspected and given a hazard score ("Hazard"). 
#  You can think of the hazard score as a continuous number that represents the liability of a property to the insurer. 
#  Some inspection hazards are major and contribute more to the total score, while some are minor and contribute less. 
#  The total score for a property is the sum of the individual hazards.
#  The aim of the competition is to forecast the hazard score based on anonymized variables describing the property."

liberty <- read_csv("~/Documents/kaggle/liberty_mutual/train.csv")

### look at the correlation between Hazard metric and other numerical metric
numeric_liberty <- liberty[,sapply(liberty, is.numeric)]
correlation <- cor(numeric_liberty[,-1])
# There is no strong correlation between numeric columns and Hazard

plot(liberty$Hazard, liberty$T1_V9)

liberty <- as.data.frame(unclass(liberty))
library("randomForest")
smp_size <- floor(.8 * nrow(liberty))
set.seed(1234)
train_indices <- sample(seq_len(nrow(liberty)), size=smp_size)
train_liberty <- liberty[train_indices, ]
test_liberty <- liberty[-train_indices, ]

hazard <- test_liberty[,2]
test_liberty <- test_liberty[,-2]

fit.forest <- randomForest(Hazard ~ T1_V1 + T1_V2 + T1_V3 + T1_V4 + T1_V5 + T1_V6 + T1_V7 + T1_V8 + T1_V9 + T1_V10 +
                                    T1_V11 + T1_V12 + T1_V13 + T1_V14 + T1_V15 + T1_V16 + T1_V17 + T2_V1 + T2_V2 +
                                    T2_V3 + T2_V4 + T2_V5 + T2_V6 + T2_V7 + T2_V8 + T2_V9 + T2_V10 + T2_V11 +T2_V12 +
                                    T2_V13 + T2_V14 + T2_V15, data=train_liberty, importance=TRUE, ntree=1001)
